function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var stage_memorize = 0;
var stage_input_ccn = 1;
var stage_input_cvc = 2;
var stage_input_expr = 3;
var stage_reset = 4;
var stage = stage_memorize;

failure_words = [
    "Bollocks",
    "Bugger",
    "ffs",
    "Shite",
    "Damn",
    "Goddammit",
    "Gah",
    "Nope",
    "Boo",
    "Dagnabbit",
    "For goodness' sake",
    "Nuh-uh",
    "Nah",
    "Negative",
]

success_words = [
    "Woohooo!",
    "No way",
    "PARTY!",
    "Hell yeah",
    "Well done",
    "Good job",
    "Awww yeah",
    "Indeed",
    "Hurrah",
    "Whoopee",
    "Hooray",
    "You're a master",
    "Nice",
    "Awesome",
    "Amazing",
    "Keep up the good work",
    "Keep it up",
]

var names = [
    "John Smith",
    "Gamithra M.",
    "Donald Trump",
    "Foo Bar",
    "Jane Doe",
    "David",
    "Karl Marx",
    "Abe Lincoln",
    "Elon Musk",
    "Steve Jobs",
    "S. Wozniak",
    "Willy Wonka",
    "Bill Gates",
    "Henry Ford",
]

var colors = [
    "blue",
    "brown",
    "Chocolate",
    "Crimson",
    "DarkGreen",
    "DarkRed"
]

var ccn;
var cvc;
var expr;
var name;
var score;

function failure() {
    return `<span class="failure">`+failure_words[Math.floor(Math.random()*failure_words.length)]+`!</span>`;
}
function success() {
    return `<span class="success">`+success_words[Math.floor(Math.random()*success_words.length)]+`!</span>`;
}
function enter() {
    if (stage == stage_memorize) {
        document.getElementById("section-cards").style.display = "none";
        document.getElementById("section-input").style.display = "block";
        document.getElementById("ccn-question").style.display = "inline-block";
        document.getElementById("ccn-format").style.display = "block";
        document.getElementById("ccn-input").style.display = "block";
        document.getElementById("ccn-input").focus()
        stage = stage_input_ccn;
    } else if (stage == stage_input_ccn) {
        let input_ccn = document.getElementById("ccn-input").value;
        let ccn_score = 0;
        for (let i=0; i < (Math.min(input_ccn.length, ccn.length)); i++) {
            if (ccn[i] != "-") {
                if (ccn[i] == input_ccn[i]) {
                    ccn_score += 1
                }
            }
        }
        if (input_ccn == ccn) {
            document.getElementById("ccn-response").innerHTML = success() + " You got it right!";
        } else if (ccn_score > 0){
            document.getElementById("ccn-response").innerHTML = failure() + " It was " + ccn + ", but you got " + ccn_score + " digits right!";
        }
        else {
            document.getElementById("ccn-response").innerHTML = failure() + " It was " + ccn + ".";
        }
        score += ccn_score;
        document.getElementById("ccn-response").style.display = "block";
        document.getElementById("cvc-question").style.display = "inline-block";
        document.getElementById("cvc-format").style.display = "block";
        document.getElementById("cvc-input").style.display = "block";
        document.getElementById("cvc-input").focus()
        stage = stage_input_cvc;
    } else if (stage == stage_input_cvc) {
        let input_cvc = document.getElementById("cvc-input").value;
        let cvc_score = 0;
        for (let i=0; i < (Math.min(input_cvc.length, cvc.length)); i++) {
            if (cvc[i] == input_cvc[i]) {
                cvc_score += 1;
            }
        }
        if (input_cvc == cvc) {
            document.getElementById("cvc-response").innerHTML = success() + " That's correct!";
        } else if (cvc_score > 0){
            document.getElementById("cvc-response").innerHTML = failure() + " It was " + cvc + ", but you got " + cvc_score + " digits right!";
        }
        else {
            document.getElementById("cvc-response").innerHTML = failure() + " It was " + cvc + ".";
        }
        score += cvc_score;
        document.getElementById("cvc-response").style.display = "block";
        document.getElementById("expr-question").style.display = "inline-block";
        document.getElementById("expr-format").style.display = "block";
        document.getElementById("expr-input").style.display = "block";
        document.getElementById("expr-input").focus()
        stage = stage_input_expr;
    } else if (stage == stage_input_expr) {
        let input_expr = document.getElementById("expr-input").value;
        let expr_score = 0;
    
        if (expr.substring(0, 2) == input_expr.substring(0, 2)) {
            expr_score += 1
        }
        if (expr.substring(3, 5) == input_expr.substring(3, 5)) {
            expr_score += 1
        }

        if (input_expr == expr) {
            document.getElementById("expr-response").innerHTML = success() + " That's it.";
        } else if (expr_score > 0){
            document.getElementById("expr-response").innerHTML = failure() + " It was " + expr + ", but you got " + expr_score + " digits right!";
        }
        else {
            document.getElementById("expr-response").innerHTML = failure() + " It was " + expr + ".";
        }
        score += expr_score;
        document.getElementById("expr-response").style.display = "block";
        document.getElementById("results").innerHTML = "Score for this round: " + Math.round(score / 21 * 100) + "%";
        document.getElementById("results").style.display = "inline-block";
        document.getElementById("info").style.display = "block";
        document.getElementById("reset").style.display = "block";
        stage = stage_reset;
    } else if (stage == stage_reset) {
        reset();
    }
}

function reset() {
    ccn = getRandomInt(1000, 9999) + "-" + 
    getRandomInt(1000, 9999) + "-" +
    getRandomInt(1000, 9999) + "-" +
    getRandomInt(1000, 9999);
    cvc = getRandomInt(100, 999);
    expr = getRandomInt(1, 12).toString().padStart(2, "0") + "/" + getRandomInt(20, 28);
    name = names[Math.floor(Math.random()*names.length)];

    document.getElementById("card-num").innerText = ccn;
    document.getElementById("card-cvc").innerText = cvc;
    document.getElementById("card-expr").innerText = expr;
    document.getElementById("card-name").innerText = name;

    document.body.style.setProperty('--card-color', colors[Math.floor(Math.random()*colors.length)]);

    document.getElementById("section-cards").style.display = "block";
    document.getElementById("section-input").style.display = "none";

    document.getElementById("ccn-question").style.display = "none";
    document.getElementById("ccn-format").style.display = "none";
    document.getElementById("ccn-input").style.display = "none";
    document.getElementById("ccn-input").value = "";
    document.getElementById("ccn-response").style.display = "none";

    document.getElementById("cvc-question").style.display = "none";
    document.getElementById("cvc-format").style.display = "none";
    document.getElementById("cvc-input").style.display = "none";
    document.getElementById("cvc-input").value = "";
    document.getElementById("cvc-response").style.display = "none";

    document.getElementById("expr-question").style.display = "none";
    document.getElementById("expr-format").style.display = "none";
    document.getElementById("expr-input").style.display = "none";
    document.getElementById("expr-input").value = "";
    document.getElementById("expr-response").style.display = "none";

    document.getElementById("results").style.display = "none";
    document.getElementById("info").style.display = "none";
    document.getElementById("reset").style.display = "none";

    score = 0;
    stage = stage_memorize;
}

window.addEventListener("load", function () {
    reset();
});

document.addEventListener('keypress', function (e) {
    if (e.keyCode == 13) {
        enter();
    }
})
